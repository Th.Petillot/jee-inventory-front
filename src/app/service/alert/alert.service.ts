import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
    errors: string[];
    warnings: string[];
    successes: string[];

    constructor() {
        this.errors = [];
        this.warnings = [];
        this.successes = [];
    }

    addError(message: string) {
        this.add(this.errors, message);
    }

    addWarning(message: string) {
        this.add(this.warnings, message);
    }

    addSuccess(message: string) {
        this.add(this.successes, message);
    }

    private add(stack: string[], message: string) {
        stack.push(message);
        setTimeout(() => {
            stack.pop();
        },3000);
    }
}
