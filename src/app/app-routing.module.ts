import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './component/login/login.component';
import {HomeComponent} from './component/home/home/home.component';
import {InventoriesComponent} from './component/inventory/inventories/inventories.component';
import {ProductListComponent} from './component/inventory/products/list/product-list.component';
import {InventoryDetailComponent} from './component/inventory/inventory-detail/inventory-detail.component';
import {DashboardComponent} from './component/inventory/products/dashboard/dashboard.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'products', component: ProductListComponent },
    { path: 'inventories', component: InventoriesComponent },
    { path: 'home', component: HomeComponent},
    { path: 'products', component: DashboardComponent},
    { path: 'inventory/:invId', component: InventoryDetailComponent},
    { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
