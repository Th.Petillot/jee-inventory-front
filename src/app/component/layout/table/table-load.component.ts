import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'table-load',
    templateUrl: './table-load.component.html',
    styleUrls: ['./table-load.component.css']
})
export class TableLoadComponent implements OnInit {

    constructor() { }

    items: any[];

    ngOnInit() {
    }

    @Input()
    set ngItems(items: any[]) {
        this.items = items;
    }

}
