import {Component, Input, OnInit} from '@angular/core';
import {InventoryResponse} from '../../../api/domain/inventory/inventory-response';
import {InventoryService} from '../../../api/service/inventory.service';

@Component({
  selector: 'app-inventory-edit',
  templateUrl: './inventory-edit.component.html',
  styleUrls: ['./inventory-edit.component.css']
})
export class InventoryEditComponent implements OnInit {

    @Input() inventory: InventoryResponse;

    constructor(public inventoryService: InventoryService) { }

    ngOnInit() {
    }

    open(inventory: InventoryResponse): void {
        console.log(inventory);
        this.inventoryService.openInventory(inventory)
            .subscribe((invUpdated) => {
                console.log('Open ' + invUpdated);
                this.inventoryService.inventories[this.inventoryService.selectedIndex] = invUpdated;
                this.inventoryService.selectedInventory = invUpdated;
            });
    }

    close(inventory: InventoryResponse): void {
        console.log(inventory);
        this.inventoryService.closeInventory(inventory)
            .subscribe((invUpdated) => {
                console.log('Close ' + invUpdated);
                this.inventoryService.inventories[this.inventoryService.selectedIndex] = invUpdated;
                this.inventoryService.selectedInventory = invUpdated;
            });
    }
}
