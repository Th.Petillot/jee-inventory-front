import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from '../../../../api/service/product.service';
import {ProductResponse} from '../../../../api/domain/product/product-response';

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

    constructor(public productService: ProductService) { }

    ngOnInit() {
        this.getProducts();
    }

    getProducts() {
        this.productService.allProducts()
            .subscribe(products => {
                this.productService.products = products;
            });
    }

    onSelect(index: number): void {
        const product: ProductResponse = this.productService.products[index];
        this.productService.selectedIndex = index;
        this.productService.selectedProduct = {id: product.id, name: product.name};
    }
}
