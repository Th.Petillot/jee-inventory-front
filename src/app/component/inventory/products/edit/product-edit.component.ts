import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ProductListComponent} from '../list/product-list.component';
import {ProductResponse} from '../../../../api/domain/product/product-response';
import {ProductService} from '../../../../api/service/product.service';
import {AlertService} from '../../../../service/alert/alert.service';
import {ExceptionService} from '../../../../api/service/exception/exception.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  @Input() product: ProductResponse;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private productComponent: ProductListComponent,
              private alertService: AlertService,
              private exceptionService: ExceptionService) { }

  ngOnInit() {
  }

  save(): void {
      this.productService.updateProduct(this.product)
          .subscribe(
              () => {
                  this.productService.products[this.productService.selectedIndex] =
                      this.productService.selectedProduct;
                  this.alertService.addSuccess('Product updated.');
              }
          );
  }

  remove(): void {
      this.productService.removeProduct(this.product)
          .subscribe(
              () => {
                  this.productService.products.splice(this.productService.selectedIndex, 1);
                  delete this.productService.selectedIndex;
                  delete this.productService.selectedProduct;
                  this.alertService.addSuccess('Product deleted.');
            }
          );
  }
}
