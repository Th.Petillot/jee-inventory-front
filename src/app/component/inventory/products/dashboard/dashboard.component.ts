import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../../../api/service/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public productService: ProductService) { }

  ngOnInit() {
      this.productService.allProducts()
          .subscribe((inv) => {
              this.productService.products = inv;
          });
  }

}
