import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../../../api/service/product.service';
import {ProductRequest} from '../../../../api/domain/product/product-request';
import {AlertService} from '../../../../service/alert/alert.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

    productRequest: ProductRequest = new class implements ProductRequest {
        name: string;
    };

    constructor(private productService: ProductService,
                private alertService: AlertService) { }

    ngOnInit() {
    }


    add(): void {
        this.productService.addProduct(this.productRequest)
            .subscribe(
                (product) => {
                    console.log('Added ' + product);
                    this.productService.products.push(product);
                    this.productRequest.name = '';
                    this.alertService.addSuccess('Product added.');
                }
            );
    }

}
