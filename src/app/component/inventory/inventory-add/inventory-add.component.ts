import { Component, OnInit } from '@angular/core';
import {InventoryService} from '../../../api/service/inventory.service';

@Component({
  selector: 'app-inventory-add',
  templateUrl: './inventory-add.component.html',
  styleUrls: ['./inventory-add.component.css']
})
export class InventoryAddComponent implements OnInit {

  constructor(public inventoryService: InventoryService) { }

  ngOnInit() {
  }

  create(): void {
      this.inventoryService.createInventory()
          .subscribe((inventory) => {
              console.log('Added ' + inventory);
              this.inventoryService.inventories.push(inventory);
          });
  }

}
