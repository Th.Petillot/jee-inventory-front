import { Component, OnInit } from '@angular/core';
import {InventoryService} from '../../../api/service/inventory.service';
import {ProductService} from '../../../api/service/product.service';

@Component({
  selector: 'app-inventory-detail',
  templateUrl: './inventory-detail.component.html',
  styleUrls: ['./inventory-detail.component.css']
})
export class InventoryDetailComponent implements OnInit {

  constructor(private inventoryService: InventoryService,
              private productService: ProductService) { }

  ngOnInit() {
      this.getProduct();
  }

  getProduct(): void {
      this.inventoryService.getProduct(this.inventoryService.selectedInventory);
  }

}
