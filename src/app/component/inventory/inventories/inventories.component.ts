import { Component, OnInit } from '@angular/core';
import {InventoryService} from '../../../api/service/inventory.service';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.css']
})
export class InventoriesComponent implements OnInit {

    constructor(public inventoryService: InventoryService) { }

    ngOnInit() {
        this.getInventories();
    }

    getInventories(): void {
        this.inventoryService.getInventories()
            .subscribe(inventories => {
                    this.inventoryService.inventories = inventories;
                }
            );
    }

    onSelect(index: number): void {
        this.inventoryService.selectedIndex = index;
        this.inventoryService.selectedInventory = this.inventoryService.inventories[index];
    }
}
