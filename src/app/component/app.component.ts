import { Component } from '@angular/core';
import {AuthService} from '../api/service/auth.service';
import {environment} from '../environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private authService: AuthService;

  public constructor(authService: AuthService) {
      this.authService = authService;
      this.authService.jwtToken = environment.getJwtToken();
  }

  public logout() {
      this.authService.logout();
  }
}
