import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../api/service/auth.service';
import {JwtAuthenticationRequest} from '../../api/domain/auth/jwt-authentication-request';
import {Router} from '@angular/router';
import {AlertService} from '../../service/alert/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    jwtAuthenticationRequest: JwtAuthenticationRequest;

    constructor(private authService: AuthService,
                private alertService: AlertService,
                private router: Router) { }

    ngOnInit() {
        this.jwtAuthenticationRequest = {
            password: '',
            username: ''
        };
    }

    // Login action
    public login() {
        this.authService.login(this.jwtAuthenticationRequest).subscribe(
        () => {
            this.router.navigate(['/home']);
            this.alertService.addSuccess('Successfully connected!');
        },
        exception => {
            this.alertService.addError(exception.error)
            console.log(this.alertService.errors);
        }
        );
    }
}
