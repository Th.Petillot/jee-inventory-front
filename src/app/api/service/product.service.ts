import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductResponse} from '../domain/product/product-response';
import {environment} from '../../environment';
import {Observable} from 'rxjs';
import {ProductRequest} from '../domain/product/product-request';
import {ExceptionService} from './exception/exception.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
    readonly endpoint = {
        products: '/product/all',
        product: '/product',
    };

    products: ProductResponse[];
    selectedProduct: ProductResponse;
    selectedIndex: number;

    constructor(public http: HttpClient, private exceptionService: ExceptionService) {
    }

    allProducts() {
        this.products = undefined;
        const response = this.http.get<Array<ProductResponse>>(
            environment.inventory.url + this.endpoint.products,
            environment.inventory.option
        );

        this.handleException(response);
        response.subscribe(undefined, () => this.products = []);
        return response;
    }

    updateProduct(prod: ProductResponse): Observable<ProductResponse> {
        const response = this.http.put<ProductResponse>(
            environment.inventory.url + this.endpoint.product + '/' + prod.id,
            prod,
            environment.inventory.option
        );

        this.handleException(response);

        return response;
    }

    removeProduct(prod: ProductResponse): Observable<ProductResponse> {
        const response = this.http.delete<ProductResponse>(
            environment.inventory.url + this.endpoint.product + '/' + prod.id,
            environment.inventory.option
        );

        this.handleException(response);

        return response;
    }

    addProduct(productRequest: ProductRequest): Observable<ProductResponse> {
        const response = this.http.post<ProductResponse>(
            environment.inventory.url + this.endpoint.product,
            productRequest,
            environment.inventory.option
        );

        this.handleException(response);

        return response;
    }

    getProduct(id: number): Observable<ProductResponse> {
        const response = this.http.get<ProductResponse>(
            environment.inventory.url + this.endpoint.product + '/' + id,
            environment.inventory.option
        );

        this.handleException(response);

        return response;
    }

    private handleException(response: Observable<any>) {
        response.subscribe(null,
            exception => this.exceptionService.handleException(exception)
        );
    }
}
