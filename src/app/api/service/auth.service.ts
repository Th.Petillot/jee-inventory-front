import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environment';
import {JwtAuthenticationRequest} from '../domain/auth/jwt-authentication-request';
import {JwtAuthenticationResponse} from '../domain/auth/jwt-authentication-response';
import {Observable} from 'rxjs';
import {JwtToken} from './auth/jwt-token';
import {Router} from '@angular/router';
import {AlertService} from '../../service/alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    readonly endpoint = {
        auth: '/auth',
        refresh: '/refresh'
    };

    private _jwtToken: JwtToken;

    isLoggedIn: boolean;

    constructor(private alertService: AlertService,
                private http: HttpClient, private router: Router) { }

    login(body: JwtAuthenticationRequest): Observable<JwtAuthenticationResponse> {
        const response: Observable<JwtAuthenticationResponse> =
            this.http.post<JwtAuthenticationResponse>(
                environment.inventory.url + this.endpoint.auth,
                JSON.stringify(body),
                environment.inventory.option
        );

        response.subscribe(
            (data: JwtAuthenticationResponse) => {
                this.handleJwtAuthenticationResponse(data);
            },
            _ => _
        );
        return response;
    }

    logout() {
        environment.unsetJwtToken();
        this.jwtToken = undefined;
        this.router.navigate(['/login']);
        this.alertService.addSuccess("Successfully logout!");
    }

    refresh() {
        this.http.post<JwtAuthenticationResponse>(
            environment.inventory.url + this.endpoint.refresh,
            null,
            environment.inventory.option
        ).subscribe(
            data => this.handleJwtAuthenticationResponse(data),
            _ => this.logout()
        );
    }

    hasAuthority(authority: string): boolean {
        return this.jwtToken && this.jwtToken.authorities ?
            this.jwtToken.authorities.includes(authority) : false;
    }

    // Setter & Getter
    public reloadLoggedIn()  {
        let loggedInState = this.getLoggedInState();
        if (loggedInState != this.isLoggedIn) {
            this.isLoggedIn = loggedInState;
        }
    }

    get jwtToken(): JwtToken {
        return this._jwtToken;
    }

    set jwtToken(value: JwtToken) {
        this._jwtToken = value;
        this.reloadLoggedIn();
    }


    // Util methods
    private getLoggedInState(): boolean {
        let defined = this._jwtToken != undefined;
        let expired = true;
        if (defined) {
            expired = this.isTokenExpired();
        }

        if (defined && expired) {
            //this.refresh();
         }

        return defined && !expired;
    }

    private isTokenExpired() {
        return new Date().getTime() > this._jwtToken.expired.getTime();
    }

    private handleJwtAuthenticationResponse(data: JwtAuthenticationResponse) {
        this._jwtToken = new JwtToken(
            data.token,
            data.authorities,
            new Date(data.expiration)
        );
        environment.setJwtToken(this._jwtToken);
        this.reloadLoggedIn();
    }
}
