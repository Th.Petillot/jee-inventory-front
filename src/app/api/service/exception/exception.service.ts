import { Injectable } from '@angular/core';
import {AlertService} from '../../../service/alert/alert.service';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExceptionService {

  constructor(private alertService: AlertService) { }

  handleException(exception: HttpErrorResponse) {
      switch(exception.status) {
          case 0:
              this.alertService.addError("Unknow Error");
              break;
          default:
              for (let error of exception.error.errors) {
                  console.log(this.alertService)
                  this.alertService.addError(error.defaultMessage);
              }
      }
  }
}
