export class JwtToken {
    private _token: string;
    private _authorities: string[];
    private _expired: Date;

    constructor(token?: string, authorithies?: Array<string>, expired?: Date) {
        this.token = token;
        this.authorities = authorithies;
        this.expired = expired;
    }

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }

    get authorities(): Array<string> {
        return this._authorities;
    }

    set authorities(value: Array<string>) {
        this._authorities = value;
    }

    get expired(): Date {
        return this._expired;
    }

    set expired(value: Date) {
        this._expired = value;
    }
}
