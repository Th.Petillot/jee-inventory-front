import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {InventoryResponse} from '../domain/inventory/inventory-response';
import {Observable} from 'rxjs';
import {environment} from '../../environment';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {


    readonly endpoint = {
        inventories: '/inventory/all',
        inventory: '/inventory',
        openInv: '/inventory/start',
        openClose: '/inventory/close',
        invProduct: '/quantity'
    };

    inventories: InventoryResponse[];
    selectedInventory: InventoryResponse;
    selectedIndex: number;

    constructor(public http: HttpClient) { }

    getInventories(): Observable<InventoryResponse[]> {
        const response = this.http.get<Array<InventoryResponse>>(
          environment.inventory.url + this.endpoint.inventories,
          environment.inventory.option
        );
      return response;
    }

    createInventory(): Observable<InventoryResponse> {
        const response = this.http.post<InventoryResponse>(
            environment.inventory.url + this.endpoint.inventory,
            null,
            environment.inventory.option
        );
        return response;
    }

    openInventory(inventory: InventoryResponse): Observable<InventoryResponse> {
        const response = this.http.post<InventoryResponse>(
            environment.inventory.url + this.endpoint.openInv + '/' + inventory.id,
            null,
            environment.inventory.option
        );
        return response;
    }

    closeInventory(inventory: InventoryResponse): Observable<InventoryResponse> {
        const response = this.http.post<InventoryResponse>(
            environment.inventory.url + this.endpoint.openClose + '/' + inventory.id,
            null,
            environment.inventory.option
        );
        return response;
    }

    getProduct(inventory: InventoryResponse): Observable<InventoryResponse> {
        const response = this.http.post<InventoryResponse>(
            environment.inventory.url + this.endpoint.invProduct + '/' + inventory.id,
            null,
            environment.inventory.option
        );
        return response;
    }
}

