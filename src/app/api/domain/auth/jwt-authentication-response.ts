export interface JwtAuthenticationResponse {
    token: string;
    authorities: Array<string>;
    expiration: Date;
}
