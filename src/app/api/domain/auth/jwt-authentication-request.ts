export interface JwtAuthenticationRequest {
    password: string;
    username: string;
}
