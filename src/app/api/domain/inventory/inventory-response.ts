export interface InventoryResponse {
    id: number;
    state: string;
    creationDate: string;
    closeDate: string;
}
