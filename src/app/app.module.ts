import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './component/app.component';
import { LoginComponent } from './component/login/login.component';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { ProductAddComponent } from './component/inventory/products/add/product-add.component';
import { HomeComponent } from './component/home/home/home.component';
import { ProductListComponent } from './component/inventory/products/list/product-list.component';
import { TokenInterceptor } from './interceptor/token.interceptor';
import { InventoriesComponent} from './component/inventory/inventories/inventories.component';
import { ProductEditComponent } from './component/inventory/products/edit/product-edit.component';
import { TableLoadComponent } from './component/layout/table/table-load.component';
import { AlertComponent } from './component/alert/alert.component';
import { InventoryEditComponent } from './component/inventory/inventory-edit/inventory-edit.component';
import { InventoryAddComponent } from './component/inventory/inventory-add/inventory-add.component';
import { InventoryDetailComponent } from './component/inventory/inventory-detail/inventory-detail.component';
import { DashboardComponent } from './component/inventory/products/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InventoriesComponent,
    HomeComponent,
    ProductListComponent,
    ProductEditComponent,
    TableLoadComponent,
    ProductAddComponent,
    AlertComponent,
    InventoryEditComponent,
    InventoryAddComponent,
    InventoryDetailComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
      {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

