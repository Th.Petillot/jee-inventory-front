import {HttpHeaders} from '@angular/common/http';
import {JwtToken} from './api/service/auth/jwt-token';

export const environment = {
    whitelist: ['localhost:8080'],
    setJwtToken: (jwtToken: JwtToken) => {
        localStorage.setItem('access_token', jwtToken.token);
        localStorage.setItem('expired_token', jwtToken.expired.toDateString());
        let authorities = '';
        for (let authority of jwtToken.authorities) {
            authorities += authority + ',';
        }
        authorities = authorities.slice(0, -1);
        localStorage.setItem('authorities_token', authorities);
    },
    getJwtToken: (): JwtToken => {
        let jwtToken: JwtToken = new JwtToken();
        jwtToken.token = localStorage.getItem('access_token');
        jwtToken.expired = new Date(localStorage.getItem('expired_token'));
        let authorities: string = localStorage.getItem('authorities_token');
        if (authorities) {
            jwtToken.authorities = authorities.split(',');
        }

        return jwtToken;
    },
    unsetJwtToken: () => {
        localStorage.removeItem('access_token');
        localStorage.removeItem('expired_token');
        localStorage.removeItem('authorities_token');
    },
    timeBeforeRefresh: 10800,
    inventory: {
        url: 'http://localhost:8080',
        option: {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        }
    }
};
