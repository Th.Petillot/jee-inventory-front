import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environment';
import {AuthService} from '../api/service/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.authService.reloadLoggedIn();
        if (this.authService.isLoggedIn && this.isWhitelisted(request.url)) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.authService.jwtToken.token}`
                }
            });
        }
        return next.handle(request);
    }

    private isWhitelisted(url: string) {
        let whitelisted: boolean = false;
        for (let white of environment.whitelist) {
            whitelisted = whitelisted || url.includes(white);
        }
        return whitelisted;
    }
}
